import java.io.*;
import java.net.*;
import java.util.ArrayList;

public class ServidorAritmeticaMU {
   ArrayList<ServidorThread> servidores = 
         new ArrayList<ServidorThread>();
	int porta = 6789;
   int status = 0;
   Desafio desafio;
	ServerSocket conexaoServidor;

   public ServidorAritmeticaMU(int porta) {
      try {
         this.conexaoServidor = new ServerSocket(porta);         
   		System.out.println("Esperando conexoes na porta " + porta);
      } catch (Exception e) {
         e.printStackTrace();
      }
   }

   ServerSocket getConexaoServidor() {
      return conexaoServidor;
   }

   ArrayList<ServidorThread> getServidores() {
      return servidores;
   }

   Desafio getDesafio() {
      return desafio;
   }

   Desafio lancarDesafio() {
      for (ServidorThread servidor: getServidores()) {
         servidor.notificar(desafio.imprimir()+" ? ");
      }      
      return getDesafio();
   }
   
   int getStatus() {
      return status;
   }
   
   void iniciar() {
      status = 1;
      desafio = new Desafio();
      for (ServidorThread servidor: servidores) 
         servidor.notificar("O jogo comecou!\n");
   }

	public static void main(String argv[]) throws Exception  {   		
      ServidorAritmeticaMU servidorAMU = new ServidorAritmeticaMU(6789);
		while (true) {
         if ( servidorAMU.getStatus() == 0 ) {
   			Socket conexao = servidorAMU.getConexaoServidor().accept();
            ServidorThread st = new ServidorThread( conexao, servidorAMU );
            servidorAMU.getServidores().add(st);
            st.start();
         }
		}
	}
}
