import java.util.Random; //Classe que gera números pseudoaletórios.

public class Desafio {
   public String desafio, resposta;
   Random cria_valor = new Random(); //Criando atributo da Classe Random.
   public Desafio() {
      int operando1 = (int) (cria_valor.nextInt(1000) * Math.random()); //O método nextInt() produz um número aleatório entre 0 e 999.
      StringBuffer sb = new StringBuffer();
      sb.append(operando1);
      sb.append("+");
      int operando2 = (int) (cria_valor.nextInt(1000) * Math.random());
      sb.append(operando2);
      int resultado = operando1+operando2;
      this.desafio = new String(sb);
      this.resposta = new String(""+resultado);
   }
   public String imprimir() {
      return desafio;
   }
}